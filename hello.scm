; https://github.com/munificent/craftinginterpreters/blob/master/test/closure/closed_closure_in_function.lox

(define f #f)

((lambda ()
   (define local 1)
   (define (f_)
     (display local))
   (set! f f_)))

(f)


(fffo
 (ddssd))

(define (fx)
  (+ x x))

(defin)

(def)

(define (f x)
  (+ 2 3 4 x))
