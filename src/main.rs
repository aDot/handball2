mod hir;
mod parser;

use anyhow::bail;
use fs_err as fs;

type Error = String;

#[derive(Debug, PartialEq, logos::Logos)]
enum Kind {
    #[token("(")]
    Lparen,

    #[token(")")]
    Rparen,

    #[regex(r"[A-Za-z0-9!$%&*+-./:<=>?@^_~]+")]
    // Most literals are floats, and can be seperated later
    Symbol,

    #[regex(r"#(f|t)")]
    Boolean,

    #[regex(r#""(\\.|[^"\\])*""#)]
    String,

    #[error]
    #[regex(r"[ \t\n\f\r]+", logos::skip)]
    #[regex(r";.*", logos::skip)]
    Error,
}

fn main() -> anyhow::Result<()> {
    let arg = std::env::args()
        .nth(1)
        .ok_or_else(|| anyhow::anyhow!("Useage: handball sources.scm"))?;

    let src = fs::read_to_string(&arg)?;

    let (trees, errs) = dbg!(parser::parse(&src));

    if !errs.is_empty() {
        bail!("Got some parse errors");
    }

    for tree in trees {
        dbg!(hir::Lowerer::default().ast(&tree));
    }

    Ok(())
}

#[derive(Debug)]
enum Ast<'s> {
    Tree(Vec<Ast<'s>>),
    Leaf(&'s str),
}
