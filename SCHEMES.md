## Chez

- `chezscheme`
- `M-x run-chez`
- 
    ```lisp
    > (expand '(define (f x) (cond (a b) (c d) (else e))))
    (begin
    (set! f
        (lambda (#{x kbvpqn6dj3c6k95f21kwqmji8-1})
        (if a b (if c d e))))
    (#2%void))
    ```

## Chicken

- `csi`
- 
    ```lisp
    #2> (import expand-full) ; May need to be `chicken-installed` first
    ; loading /var/lib//chicken/11/expand-full.import.so ...
    ; loading /var/lib//chicken/11/expand-full.so ...
    ; loading /var/lib//chicken/11/srfi-1.so ...
    #3> (ppexpand* '(define (f x) (cond (a b) (c d) (else e))))
    (##core#begin
    (##core#ensure-toplevel-definition f)
    (##core#set!
        f
        (##core#lambda
        (x)
        (##core#if
            a
            (##core#begin b)
            (##core#if c (##core#begin d) (##core#begin e))))))
    ```

## Racket

`racket 


## Chibi

- `chibi-scheme`
- `M-x run-chibi`
- 
    ```lisp
    > (import (chibi ast))
    > (macroexpand '(define (f x) (cond (a b) (c d) (else g))))
    (set! f (lambda (x) (if a b (if c d g))))
    ```

## Gambit

## Gauche

## Guile

## MIT-scheme

## Racket

